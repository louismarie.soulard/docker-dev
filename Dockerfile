FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update
RUN apt-get install -y curl

RUN apt-get install -y nodejs npm lsof
RUN npm install --global yarn
RUN npm install --global n
RUN n 14.15.2

RUN useradd -ms /bin/bash dev
RUN yarn config set cache-folder /home/dev/.config/yarn

USER dev

WORKDIR /usr/src/app

